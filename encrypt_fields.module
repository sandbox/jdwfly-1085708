<?php
// $Id$

/**
 * Implementation of hook_form_alter().
 */
function encrypt_fields_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'content_field_edit_form') {
    encrypt_fields_admin_form($form);
  }
}

/**
 * Additional form fields to set encryption
 */
function encrypt_fields_admin_form(&$form) {
  $form['widget']['encrypt'] = array(
    '#type' => 'checkbox',
    '#title' => t('Encrypt this field'),
    '#default_value' => _encrypt_fields_get_default_value($form['type_name']['#value'], $form['field_name']['#value'], 'encrypt'),
    '#description' => t('WARNING: Only affects newly created nodes'),
  );
  $form['#submit'][] = 'encrypt_fields_admin_form_submit';
}

/**
 * Additional submit handler for content_field_edit_form
 * writes encryption setting and clears the cache
 */
function encrypt_fields_admin_form_submit($form_id, &$form_state) {
  $field = content_fields($form_state['values']['field_name'], $form_state['values']['type_name']);
  $widget_settings = $field['widget'];
  $widget_settings['encrypt'] = $form_state['values']['encrypt'];
  
  db_query("UPDATE {". content_instance_tablename() ."} SET widget_settings = '%s'
    WHERE type_name = '%s' AND field_name = '%s'", serialize($widget_settings), $form_state['values']['type_name'], $form_state['values']['field_name']); 
  content_clear_type_cache();
}

/**
 * Helper function, returns encrypt value for widget.
 */
function _encrypt_fields_get_default_value($type_name, $field_name, $value) {
  $field = content_fields($field_name, $type_name);
  return $field['widget'][$value];
}

/**
 * Implementation of hook_nodeapi().
 */
function encrypt_fields_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'presave':
      $fields = encrypt_fields_get_fields($node->type);
      foreach ($fields as $field) {
        $d = 0;
        foreach ($node->$field['field_name'] as $delta) {
          $e = encrypt($delta['value']);
          $node->{$field['field_name']}[$d]['value'] = $e;
          $d++;
        }
      }
      break;
    case 'load':
      $fields = encrypt_fields_get_fields($node->type);
      foreach ($fields as $field) {
        $d = 0;
        foreach ($node->$field['field_name'] as $delta) {
          if (!unserialize($delta['value']) || !isset($delta['value'])) {
            // data was not encrypted in the first place do nothing or null
            continue;
          }
          $e = decrypt($delta['value']);
          $node->{$field['field_name']}[$d]['value'] = (user_access('view encrypted fields')) ? $e : t('********');
          $d++;
        }
      }
      break;
  }
}

/**
 * Helper function to get all fields that need encryption for a specific content type.
 */
function encrypt_fields_get_fields($type) {
  $info = _content_type_info();
  $fields = array();
  foreach ($info['content types'][$type]['fields'] as $field) {
    if ($field['widget']['encrypt']) {
      $fields[] = $field;
    }
  }
  return $fields;
}

/**
 * Implementation of hook_perm().
 */
function encrypt_fields_perm() {
  return array('view encrypted fields');
}